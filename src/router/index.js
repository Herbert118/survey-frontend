import Vue from "vue"
import VueRouter from "vue-router"
import { BlankLayout,AdminLayout, TenantLayout,UserLayout } from "@/layout"

import allLogin from "@/views/auth/allLogin"
import allRegister from "@/views/auth/allRegister"
import pwRetrieval from "@/views/auth/pwRetrieval"
import tenantTable from "@/views/tenant/tenantTable"
import tenantForm from "@/views/tenant/tenantForm"
import userTable from "@/views/user/userTable"
import userForm from "@/views/user/userForm"
import replierTable from "@/views/replier/replierTable"
import replierForm from "@/views/replier/replierForm"
import replierAddTable from "@/views/replier/replierAddTable"
import groupTable from "@/views/group/groupTable"
import groupForm from "@/views/group/groupForm"

import surveyTable from "@/views/survey/surveyTable"
import surveyDesigner from "@/views/survey/surveyDesigner"
import surveyPreview from "@/views/survey/surveyPreview"
import surveySender from "@/views/survey/surveySender"
import surveyAnswer from "@/views/survey/surveyAnswer"
import surveyResultTable from "@/views/survey/surveyResultTable"
import surveyAnalysis from "@/views/survey/surveyAnalysis"
import surveyProjectTable from "@/views/survey/surveyProjectTable"
import surveyCreation from "@/views/survey/surveyCreation"
import surveyLinkTable from "@/views/survey/surveyLinkTable"
import surveyForm from "@/views/survey/surveyForm"
import store from "../store"


Vue.use(VueRouter)
//TODO: 优化为更restful的风格
const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: "/",
            name: "BlankLayout",
            component: BlankLayout,
            redirect: "/allLogin",
            hidden: true,
            children: [
                {
                    path: 'allLogin',
                    name: "allLogin",
                    component: allLogin,
                }, {
                    path: 'allRegister',
                    name: 'allRegister',
                    component: allRegister
                }, {
                    path: 'pwRetrieval',
                    name: 'pwRetrieval',
                    component: pwRetrieval
                },
            ],
        },
        {
            path: "/admin",
            name: "AdminLayout",
            component: AdminLayout,
            redirect: "/admin/tenantTable",
            hidden: true,
            children: [
                {
                    path: 'tenantTable',
                    name: "tenantTable",
                    component: tenantTable,
                }, {
                    path: 'tenantForm',
                    name: 'tenantForm',
                    component: tenantForm
                }
            ],
        },{
            path: "/tenant",
            name: "TenantLayout",
            component: TenantLayout,
            redirect: "/tenant/userTable",
            hidden: true,
            children: [
                {
                    path: 'userTable',
                    name: "userTable",
                    component: userTable,
                }, {
                    path: 'userForm',
                    name: 'userForm',
                    component: userForm
                },{
                    path: 'groupTable',
                    name: "groupTable",
                    component: groupTable,
                }, {
                    path: 'groupForm',
                    name: 'groupForm',
                    component: groupForm
                },{
                    path: 'surveyTable',
                    name: "surveyTable",
                    component: surveyTable,
                },{
                    path: 'tenantForm',
                    name: 'tenantForm',
                    component: tenantForm
                }
            ],
        },{
            path: "/user",
            name: "UserLayout",
            component: UserLayout,
            redirect: "/user/groupTable",
            hidden: true,
            children: [
                {
                    path: 'groupTable',
                    name: "groupTable",
                    component: groupTable,
                }, {
                    path: 'groupForm',
                    name: 'groupForm',
                    component: groupForm
                },{
                    path: 'surveyTable',
                    name: "surveyTable",
                    component: surveyTable,
                },{
                    path: 'replierTable',
                    name: 'replierTable',
                    component: replierTable,
                }, {
                    path: 'replierForm',
                    name: 'replierForm',
                    component: replierForm,
                },{
                    path: 'replierAddTable',
                    name: 'replierAddTable',
                    component: replierAddTable,
                },{
                    path: 'userForm',
                    name: 'userForm',
                    component: userForm
                }
            ],
        },
        {
            path: '/surveyTable/',
            component: surveyTable,
        },
        {
            path: '/surveyAnalysis',
            component: surveyAnalysis,
        },
        {
            path: '/surveyDesigner/:purpose',
            component: surveyDesigner,
        },
        {
            path: '/surveyPreview',
            component: surveyPreview
        },
        {
            path: '/surveySender',
            component: surveySender,
        },
        {
            path: '/surveyAnswer',
            component: surveyAnswer,
        },
        {
            path: '/surveyResultTable',
            component: surveyResultTable,
        },
        {
            path: '/surveyProjectTable',
            component: surveyProjectTable,
        }, {
            path: '/surveyCreation',
            component: surveyCreation
        }, {
            path: '/surveyLinkTable',
            component: surveyLinkTable
        }, {
            path: '/surveyForm',
            component: surveyForm
        }
    ]
})


router.beforeEach((to, from, next) => {
    let authed = true;
    if (store.state.userToken == '' || store.state.userToken == null) {
        authed = false;
    }
    if (to.path !== '/allLogin' && !authed) next({ path: '/allLogin' })
    else next()
})

export default router;
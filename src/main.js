import Vue from 'vue'
import App from './App.vue'

import store from "@/store"
import router from "@/router"
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import './assets/css/stylesheet.css'
Vue.config.productionTip = false
Vue.use(Antd)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
